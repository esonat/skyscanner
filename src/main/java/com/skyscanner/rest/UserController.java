package com.skyscanner.rest;


import com.skyscanner.domain.SkyscannerUser;
import com.skyscanner.domain.DTO.UserDTO;
import com.skyscanner.service.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/data-service/v1/users")
public class UserController {
    protected static final Logger logger = LoggerFactory.getLogger(UserController.class.getName());
    protected UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> findAllUsers(){
        List<SkyscannerUser> users=new ArrayList<>();
        List<UserDTO> resultList=new ArrayList<>();

        try{
            users=userService.getAll();
            resultList=users.stream()
                    .map(user->convertToDto(user))
                    .collect(Collectors.toList());

        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return users!=null ? new ResponseEntity<>(resultList,HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> findById(@PathVariable("id") Integer id) {
        SkyscannerUser user;
        UserDTO result;

        try {
            user = userService.get(id);
            result=convertToDto(user);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return user != null ? new ResponseEntity<UserDTO>(result, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UserDTO> add(@RequestBody UserDTO userDTO) throws Exception{
        SkyscannerUser user=convertToEntity(userDTO);
        userService.save(user);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void update(@PathVariable("id") Integer id,@RequestBody UserDTO userDTO) throws Exception{
            SkyscannerUser user=convertToEntity(userDTO);

            userService.update(user);
    }

    private UserDTO convertToDto(SkyscannerUser user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }

    private SkyscannerUser convertToEntity(UserDTO userDTO) throws ParseException {
        SkyscannerUser user= modelMapper.map(userDTO, SkyscannerUser.class);
        return user;
    }
}

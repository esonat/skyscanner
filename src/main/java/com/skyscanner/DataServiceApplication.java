package com.skyscanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@Import({WebConfig.class, SkyscannerBeanConfiguration.class,WebSecurityConfig.class, WebDatabaseConfig.class, AuthorizationServerConfig.class, AdditionalWebConfig.class})
@SpringBootApplication(scanBasePackages = {"com.skyscanner"})
public class DataServiceApplication {
    public static void main(String[] args){
        SpringApplication.run(DataServiceApplication.class,args);
    }
}

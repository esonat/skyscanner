package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;


@SuppressWarnings("serial")
@Entity
@Table(name="order",catalog="skyscannerDB")
public class Order implements DomainObject{
    private int ID;
    private SkyscannerUser user;
    private Flight flight;
    private boolean OrderType;
    private int TotalPrice;
    private Set<Seat> seats;

    public Order() {
    }

    public Order(int ID, SkyscannerUser user, Flight flight, boolean orderType, int totalPrice) {
        this.ID = ID;
        this.user = user;
        this.flight = flight;
        OrderType = orderType;
        TotalPrice = totalPrice;
    }

    public Order(int ID, SkyscannerUser user, Flight flight, boolean orderType, int totalPrice, Set<Seat> seats) {
        this.ID = ID;
        this.user = user;
        this.flight = flight;
        OrderType = orderType;
        TotalPrice = totalPrice;
        this.seats = seats;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ORDER_ID",unique = true,nullable = false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="USER_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public SkyscannerUser getUser() {
        return user;
    }

    public void setUser(SkyscannerUser user) {
        this.user = user;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="FLIGHT_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public boolean isOrderType() {
        return OrderType;
    }

    public void setOrderType(boolean orderType) {
        OrderType = orderType;
    }

    @Column(name="TOTAL_PRICE",nullable = false)
    public int getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        TotalPrice = totalPrice;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JsonIgnore
    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }
}

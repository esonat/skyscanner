package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;


@SuppressWarnings("serial")
@Entity
@Table(name="user_roles",catalog="skyscannerDB")
public class UserRole implements DomainObject{
    private String role;
    private SkyscannerUser user;
    private Integer userRoleId;

    public UserRole() {
        super();
    }

    public UserRole(SkyscannerUser user, String role) {
        this.user = user;
        this.role = role;
    }

    @Column(name="ROLE",nullable=false)
    public String getRole() {
        return this.role;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="USER_ID",nullable=false)
    @JsonIgnore
    public SkyscannerUser getUser() {
        return this.user;
    }

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="USER_ROLE_ID",unique=true,nullable=false)
    @JsonIgnore
    public Integer getUserRoleId() {
        return this.userRoleId;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setUser(SkyscannerUser user) {
        this.user = user;
    }

    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }
}

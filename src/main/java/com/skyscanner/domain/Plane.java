package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;
import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@SuppressWarnings("serial")
@Entity
@Table(name="plane",catalog="skyscannerDB")
public class Plane implements DomainObject{

    private int ID;
    private int NumberOfSeats;
    private Company company;

    public Plane(){

    }

    public Plane(int ID, int numberOfSeats, Company company) {
        this.ID = ID;
        NumberOfSeats = numberOfSeats;
        this.company = company;
    }

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="PLANE_ID",unique=true,nullable=false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Column(name="NUMBER_OF_SEATS",nullable = false)
    public int getNumberOfSeats() {
        return NumberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        NumberOfSeats = numberOfSeats;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="COMPANY_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}

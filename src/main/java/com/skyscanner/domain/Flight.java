package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

@SuppressWarnings("serial")
@Entity
@Table(name="flight",catalog = "skyscannerDB")
public class Flight implements DomainObject{
    private int ID;
    private Airport fromAirport;
    private Airport toAirport;
    private Plane plane;
    private Set<Order> orders;
    private Set<Seat> seats;

    public Flight(){

    }

    public Flight(int ID, Airport fromAirport, Airport toAirport, Plane plane) {
        this.ID = ID;
        this.fromAirport = fromAirport;
        this.toAirport = toAirport;
        this.plane = plane;
    }

    public Flight(int ID, Airport fromAirport, Airport toAirport, Plane plane, Set<Order> orders, Set<Seat> seats) {
        this.ID = ID;
        this.fromAirport = fromAirport;
        this.toAirport = toAirport;
        this.plane = plane;
        this.orders = orders;
        this.seats = seats;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="FROM_AIRPORT_ID",referencedColumnName = "AIRPORT_ID",nullable = false)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Airport getFromAirport(){
        return fromAirport;
    }

    public void setFromAirport(Airport fromAirport){
        this.fromAirport=fromAirport;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="FLIGHT_ID",unique = true,nullable = false)
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }



    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="TO_AIRPORT_ID",referencedColumnName = "AIRPORT_ID",nullable = false)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Airport getToAirport() {
        return toAirport;
    }

    public void setToAirport(Airport toAirport) {
        this.toAirport = toAirport;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="PLANE_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Plane getPlane() {
        return plane;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "flight")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JsonIgnore
    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "flight")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JsonIgnore
    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }
}

package com.skyscanner.domain;


import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@SuppressWarnings("serial")
@Entity
@Table(name="city",catalog="skyscannerDB")
public class City implements DomainObject {
    private int ID;
    private String name;
    private Country country;

    public City() {
    }

    public City(int ID, String name, Country country) {
        this.ID = ID;
        this.name = name;
        this.country = country;
    }

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="CITY_ID",unique=true,nullable=false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Column(name="NAME",length = 10000,nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="COUNTRY_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}

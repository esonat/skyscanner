package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@SuppressWarnings("serial")
@Entity
@Table(name="airport",catalog="skyscannerDB")
public class Airport implements DomainObject{
    private int ID;
    private String name;
    private City city;
    private Set<Flight> flights;
    //private Set<Flight> fromFlights;
   // private Set<Flight> toFlights;

    public Airport() {
    }

    public Airport(int ID, City city) {
        this.ID = ID;
        this.city = city;
    }

    public Airport(int ID, String name, City city, Set<Flight> flights) {
        this.ID = ID;
        this.name = name;
        this.city = city;
        this.flights = flights;
    }

    //    public Airport(int ID, City city, Set<Flight> fromFlights) {
//        this.ID = ID;
//        this.city = city;
//        this.fromFlights = fromFlights;
//    }
//
//    public Airport(int ID, String name, City city, Set<Flight> fromFlights,Set<Flight> toFlights) {
//        this.ID = ID;
//        this.name = name;
//        this.city = city;
//        this.fromFlights=fromFlights;
//        this.toFlights=toFlights;
//    }

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="AIRPORT_ID",unique=true,nullable=false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="CITY_ID",nullable=false)
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "fromAirport")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    public Set<Flight> getFlights() {
        return flights;
    }

    public void setFlights(Set<Flight> flights) {
        this.flights = flights;
    }

    //    @OneToMany(fetch = FetchType.EAGER, mappedBy = "airport")
//    @Cascade(org.hibernate.annotations.CascadeType.ALL)
//    public Set<Flight> getFromFlights() {
//        return fromFlights;
//    }
//
//    public void setFromFlights(Set<Flight> fromFlights) {
//        this.fromFlights = fromFlights;
//    }
//
//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "airport")
//    @Cascade(org.hibernate.annotations.CascadeType.ALL)
//    public Set<Flight> getToFlights() {
//        return toFlights;
//    }
//
//    public void setToFlights(Set<Flight> toFlights) {
//        this.toFlights = toFlights;
//    }

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "airport")
//    @Cascade(org.hibernate.annotations.CascadeType.ALL)
//    public Set<Flight> getFlights() {
//        return flights;
//    }
//
//    public void setFlights(Set<Flight> flights) {
//        this.flights = flights;
//    }

    @Column(name="NAME",length = 10000,nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

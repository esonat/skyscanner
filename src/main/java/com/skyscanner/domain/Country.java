package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@SuppressWarnings("serial")
@Entity
//@Indexed
@Table(name="country",catalog="skyscannerDB")
public class Country implements DomainObject{
    private int ID;
    private String name;
    private String code;
    private Set<City> cities=new HashSet<City>(0);
    private Set<SkyscannerUser> users=new HashSet<SkyscannerUser>(0);
    private Set<Company> companies=new HashSet<Company>(0);


    public Country() {
    }

    public Country(int ID, String name, String code) {
        this.ID = ID;
        this.name = name;
        this.code = code;
    }

    public Country(int ID, String name, String code, Set<City> cities) {
        this.ID = ID;
        this.name = name;
        this.code = code;
        this.cities=cities;
    }

    public Country(int ID, String name, String code, Set<City> cities, Set<SkyscannerUser> users) {
        this.ID = ID;
        this.name = name;
        this.code = code;
        this.cities = cities;
        this.users = users;
    }

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="COUNTRY_ID",unique=true,nullable=false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Column(name="NAME",unique=true,nullable=false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="CODE",unique=true,nullable=false)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @OneToMany(fetch=FetchType.EAGER,mappedBy="country")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JsonIgnore
    public Set<City> getCities() {
        return cities;
    }

    public void setCities(Set<City> cities) {
        this.cities = cities;
    }

    @OneToMany(fetch=FetchType.EAGER,mappedBy="country")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JsonIgnore
    public Set<SkyscannerUser> getUsers() {
        return users;
    }

    public void setUsers(Set<SkyscannerUser> users) {
        this.users = users;
    }

    @OneToMany(fetch=FetchType.EAGER,mappedBy="country")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JsonIgnore
    public Set<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(Set<Company> companies) {
        this.companies = companies;
    }
}


package com.skyscanner.domain;

import com.skyscanner.validator.Username;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@SuppressWarnings("serial")
@Entity
@Table(name="user",catalog="skyscannerDB")
public class SkyscannerUser implements DomainObject {
    private boolean enabled;
    private int ID;
    @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.Firstname.validation}")
    private String firstname;

    @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.Middlename.validation}")
    private String middlename;

    @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.Lastname.validation}")
    private String lastname;

    //@Pattern(regexp="^[A-Za-z0-9_.]+$",message="{Pattern.User.password.validation}")
    @Size(min = 5, max = 150, message = "{Size.User.password.validation}")
    private String password;
    private Set<Order> orders = new HashSet<Order>(0);
    @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.username.validation}")
    @Username
    private String username;
    private Set<UserRole> userRole = new HashSet<UserRole>(0);
    //private Integer version;
    private Country country;


    public SkyscannerUser() {
        super();
    }

    public SkyscannerUser(String firstname, String middlename, String lastname) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
    }

    public SkyscannerUser(String firstname, String middlename, String lastname, String username, String password, boolean enabled) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    public SkyscannerUser(String firstname, String middlename, String lastname, String username, String password, boolean enabled, Set<UserRole> userRole) {
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.userRole = userRole;
    }

    public SkyscannerUser(boolean enabled, int ID, @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.Firstname.validation}") String firstname, @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.Middlename.validation}") String middlename, @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.Lastname.validation}") String lastname, @Size(min = 5, max = 150, message = "{Size.User.password.validation}") String password, Set<Order> orders, @Pattern(regexp = "[a-zA-Z]+", message = "{Pattern.User.username.validation}") String username, Set<UserRole> userRole, Country country) {
        this.enabled = enabled;
        this.ID = ID;
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.password = password;
        this.orders = orders;
        this.username = username;
        this.userRole = userRole;
        this.country = country;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "USER_ID", unique = true, nullable = false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    @Column(name = "FIRSTNAME", unique = true, nullable = false)
    public String getFirstname() {
        return firstname;
    }

    @Column(name = "MIDDLENAME", unique = true, nullable = true)
    public String getMiddlename() {
        return middlename;
    }

    @Column(name = "LASTNAME", unique = true, nullable = false)
    public String getLastname() {
        return lastname;
    }

    @Column(name = "PASSWORD", nullable = false)
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    @JsonIgnore
    public Set<Order> getOrders() {
        return orders;
    }

    @Column(name = "USERNAME", unique = true, nullable = false)
    public String getUsername() {
        return username;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    public Set<UserRole> getUserRole() {
        return userRole;
    }

    @Column(name = "ENABLED", nullable = false)
    @JsonIgnore
    public boolean isEnabled() {
        return enabled;
    }


    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="COUNTRY_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUserRole(Set<UserRole> userRole) {
        this.userRole = userRole;
    }
}
package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@SuppressWarnings("serial")
@Entity
@Table(name="seat",catalog="skyscannerDB")
public class Seat implements DomainObject{
    private int ID;
    private Flight flight;
    private String row;

    @Min(1)
    @Max(9)
    private int column;
    private Order order;

    @Min(1)
    @Max(3)
    private int seat_class;

    @Min(10)
    private int price;

    public Seat() {
    }

    public Seat(int ID,Flight flight, String row, int column, Order order, int seat_class, int price) {
        this.ID=ID;
        this.flight = flight;
        this.row = row;
        this.column = column;
        this.order = order;
        this.seat_class = seat_class;
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="SEAT_ID",unique = true,nullable = false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="FLIGHT_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Column(name="ROW",nullable=false)
    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    @Column(name="COLUMN",nullable=false)
    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ORDER_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Column(name="SEAT_CLASS",nullable=false)
    public int getSeat_class() {
        return seat_class;
    }

    public void setSeat_class(int seat_class) {
        this.seat_class = seat_class;
    }

    @Column(name="PRICE",nullable=false)
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

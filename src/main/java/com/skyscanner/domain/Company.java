package com.skyscanner.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@SuppressWarnings("serial")
@Entity
@Table(name="company",catalog="skyscannerDB")
public class Company implements DomainObject{
    private int ID;
    private String name;
    private Country country;
    private Set<Plane> planes;

    public Company() {
    }

    public Company(int ID, String name, Country country) {
        this.ID = ID;
        this.name = name;
        this.country = country;
    }

    public Company(int ID, String name, Country country, Set<Plane> planes) {
        this.ID = ID;
        this.name = name;
        this.country = country;
        this.planes = planes;
    }

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="COMPANY_ID",unique=true,nullable=false)
    @JsonIgnore
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Column(name="NAME",length=10000,nullable=false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="COUNTRY_ID")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "company")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    public Set<Plane> getPlanes() {
        return planes;
    }

    public void setPlanes(Set<Plane> planes) {
        this.planes = planes;
    }
}

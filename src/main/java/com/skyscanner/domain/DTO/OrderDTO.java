package com.skyscanner.domain.DTO;

import com.skyscanner.domain.SkyscannerUser;

public class OrderDTO {
    private int id;
    private SkyscannerUser user;
    private FlightDTO flight;
    private boolean orderType;
    private int totalPrice;

    public OrderDTO() {
    }

    public OrderDTO(int id, SkyscannerUser user, FlightDTO flight, boolean orderType, int totalPrice) {
        this.id = id;
        this.user = user;
        this.flight = flight;
        this.orderType = orderType;
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SkyscannerUser getUser() {
        return user;
    }

    public void setUser(SkyscannerUser user) {
        this.user = user;
    }

    public FlightDTO getFlight() {
        return flight;
    }

    public void setFlight(FlightDTO flight) {
        this.flight = flight;
    }

    public boolean isOrderType() {
        return orderType;
    }

    public void setOrderType(boolean orderType) {
        this.orderType = orderType;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}

package com.skyscanner.domain.DTO;

public class UserRoleDTO {
    private String role;
    private UserDTO user;
    private int userRoleId;

    public UserRoleDTO() {
    }

    public UserRoleDTO(String role, UserDTO user, int userRoleId) {
        this.role = role;
        this.user = user;
        this.userRoleId = userRoleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }
}

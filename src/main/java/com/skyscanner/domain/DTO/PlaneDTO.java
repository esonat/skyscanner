package com.skyscanner.domain.DTO;


public class PlaneDTO {
    private int id;
    private int numberOfSeats;
    private CompanyDTO company;

    public PlaneDTO() {
    }

    public PlaneDTO(int id, int numberOfSeats, CompanyDTO company) {
        this.id = id;
        this.numberOfSeats = numberOfSeats;
        this.company = company;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }
}

package com.skyscanner.domain.DTO;

public class SeatDTO {
    private FlightDTO flight;
    private String row;
    private OrderDTO order;
    private int seat_class;
    private int price;

    public SeatDTO() {
    }

    public SeatDTO(FlightDTO flight, String row, OrderDTO order, int seat_class, int price) {
        this.flight = flight;
        this.row = row;
        this.order = order;
        this.seat_class = seat_class;
        this.price = price;
    }

    public FlightDTO getFlight() {
        return flight;
    }

    public void setFlight(FlightDTO flight) {
        this.flight = flight;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public OrderDTO getOrder() {
        return order;
    }

    public void setOrder(OrderDTO order) {
        this.order = order;
    }

    public int getSeat_class() {
        return seat_class;
    }

    public void setSeat_class(int seat_class) {
        this.seat_class = seat_class;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

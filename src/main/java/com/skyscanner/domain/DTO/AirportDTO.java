package com.skyscanner.domain.DTO;

public class AirportDTO {
    private int id;
    private String name;
    private CityDTO city;

    public AirportDTO() {
    }

    public AirportDTO(int id, CityDTO city) {
        this.id = id;
        this.city = city;
    }

    public AirportDTO(int id, String name, CityDTO city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }
}

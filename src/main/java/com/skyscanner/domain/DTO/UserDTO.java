package com.skyscanner.domain.DTO;

import java.util.List;

public class UserDTO {
    private Integer id;
    private String firstname;
    private String middlename;
    private String lastname;
    private String username;
    private String password;
    private List<OrderDTO> orders;

    public UserDTO(){
    }

    public UserDTO(String firstname, String middlename, String lastname){
        this.firstname=firstname;
        this.middlename=middlename;
        this.lastname=lastname;
    }

    public UserDTO(String firstname, String middlename, String lastname, String username, String password){
        this.firstname=firstname;
        this.middlename=middlename;
        this.lastname=lastname;
        this.username=username;
        this.password=password;
    }

    public UserDTO(String firstname, String middlename, String lastname, String username, String password, List<OrderDTO> orders){
        this.firstname=firstname;
        this.middlename=middlename;
        this.lastname=lastname;
        this.username=username;
        this.password=password;
        this.orders=orders;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return new StringBuilder("{id: ").append(id)
                .append(", fullname: ").append(firstname+" "+middlename+" "+lastname)
                .append(", username: ").append(username)
                .append(", password: ").append(password)
                .append("}").toString();
    }
}

package com.skyscanner.domain.DTO;

public class FlightDTO {
    private int id;
    private AirportDTO fromAirport;
    private AirportDTO toAirport;
    private PlaneDTO plane;

    public FlightDTO() {
    }

    public FlightDTO(int id, AirportDTO fromAirport, AirportDTO toAirport, PlaneDTO plane) {
        this.id = id;
        this.fromAirport = fromAirport;
        this.toAirport = toAirport;
        this.plane = plane;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AirportDTO getFromAirport() {
        return fromAirport;
    }

    public void setFromAirport(AirportDTO fromAirport) {
        this.fromAirport = fromAirport;
    }

    public AirportDTO getToAirport() {
        return toAirport;
    }

    public void setToAirport(AirportDTO toAirport) {
        this.toAirport = toAirport;
    }

    public PlaneDTO getPlane() {
        return plane;
    }

    public void setPlane(PlaneDTO plane) {
        this.plane = plane;
    }
}

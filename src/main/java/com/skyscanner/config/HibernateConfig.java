package com.skyscanner.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan("com.skyscanner.dao.hibernate")
public class HibernateConfig {

    public DataSource skyscannerDataSource(){
        BasicDataSource basicDataSource=new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUrl("jdbc:mysql://localhost:3306/skyscannerDB");
        basicDataSource.setUsername("root");
        basicDataSource.setPassword("sonat");

        return basicDataSource;
    }


    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor(){
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new          LocalContainerEntityManagerFactoryBean();
        em.setDataSource(skyscannerDataSource());
        em.setPackagesToScan(new String[] { "com.skyscanner.domain" });
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        Properties props=new Properties();
        props.put("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
        props.put("hibernate.show_sql","true");
        props.put("hibernate.jdbc.batch_size",5);
        props.put("hibernate.c3p0.max_size",72);
        props.put("hibernate.c3p0.min_size",18);
        props.put("hibernate.c3p0.timeout",5000);
        props.put("hibernate.c3p0.validate","false");
        props.put("hibernate.c3p0.max_statements",500);
        props.put("hibernate.c3p0.acquire_increment",2);
        props.put("hibernate.c3p0.idle_test_period",3000);
        props.put("hibernate.current_session_context_class","thread");

        em.setJpaProperties(props);

        return em;
    }

    @Bean
    @Primary
    public LocalSessionFactoryBean sessionFactory(){
        LocalSessionFactoryBean annotationSessionFactoryBean=new LocalSessionFactoryBean();
        annotationSessionFactoryBean.setDataSource(skyscannerDataSource());
        annotationSessionFactoryBean.setPackagesToScan("com.skyscanner.domain");

        Properties props=new Properties();
        props.put("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
        props.put("hibernate.show_sql","true");
        props.put("hibernate.jdbc.batch_size",5);
        props.put("hibernate.c3p0.max_size",72);
        props.put("hibernate.c3p0.min_size",18);
        props.put("hibernate.c3p0.timeout",5000);
        props.put("hibernate.c3p0.validate","false");
        props.put("hibernate.c3p0.max_statements",500);
        props.put("hibernate.c3p0.acquire_increment",2);
        props.put("hibernate.c3p0.idle_test_period",3000);
        props.put("hibernate.current_session_context_class","thread");
        annotationSessionFactoryBean.setHibernateProperties(props);

        return annotationSessionFactoryBean;
    }

}

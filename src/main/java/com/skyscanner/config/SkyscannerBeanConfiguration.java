package com.skyscanner.config;

import com.skyscanner.dao.hibernate.UserDaoHibernate;
import com.skyscanner.users.service.MyUserDetailsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@Import({DatasourceConfig.class,HibernateConfig.class})
@ComponentScan("com.skyscanner.config")
@PropertySource(value="classpath:/application.properties")
public class SkyscannerBeanConfiguration {

    @Bean
    public MyUserDetailsService myUserDetailsService(){
        MyUserDetailsService myUserDetailsService=new MyUserDetailsService();
        myUserDetailsService.setUserDao(userDao());

        return myUserDetailsService;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor(){
        return new PersistenceExceptionTranslationPostProcessor();
    }
    @Bean
    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer(){
        //new ClassPathResource("META-INF/spring/jdbc.properties");
        //new ClassPathResource("classpath:application.properties");
        //return new PropertyPlaceholderConfigurer();
        PropertyPlaceholderConfigurer configurer=new PropertyPlaceholderConfigurer();
        configurer.setLocation(new ClassPathResource("application.properties"));
        return configurer;
    }

    @Bean
    public UserDaoHibernate userDao(){
        return new UserDaoHibernate();
    }

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

}

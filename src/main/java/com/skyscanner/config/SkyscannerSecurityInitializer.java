//package com.skyscanner.config;
//
//import org.springframework.core.annotation.Order;
//import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
//import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
//
//import javax.servlet.Filter;
//
//@Order(2)
//public class SkyscannerSecurityInitializer extends  AbstractSecurityWebApplicationInitializer {
//
//    protected Filter[] getServletFilters() {
//        OpenEntityManagerInViewFilter viewFilter=new OpenEntityManagerInViewFilter();
//
//        return new Filter[]{viewFilter};
//    }
//}
package com.skyscanner.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
//@Import({SkyscannerBeanConfiguration.class})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    //@Value("${security.signing-key}")
    private final String signingKey="MaYzkSjmkzPC57L";

    //@Value("${security.encoding-strength}")
    private final Integer encodingStrength=256;

 //   @Value("${security.security-realm}")
    private final String securityRealm="Spring Boot JWT Example Realm";
//
//    @Autowired
//    private MyUserDetailsService myUserDetailsService;

    //@Bean("authenticationManager")
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder(11);
        return bCryptPasswordEncoder;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
//        auth
//            .inMemoryAuthentication()
//                .withUser("admin").password(bCryptPasswordEncoder().encode("admin")).roles("ADMIN")
//                .and()
//                .withUser("user").password(bCryptPasswordEncoder().encode("user")).roles("USER");
            //.userDetailsService(myUserDetailsService)
           // .passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception{

            http
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .httpBasic()
                    .realmName(securityRealm)
                    .and()
                    .csrf()
                    .disable();

//            http
//                .csrf().disable()
//                .exceptionHandling()
//                .authenticationEntryPoint(restAuthenticationEntryPoint)
//                .and()
//                .authorizeRequests()
//                .antMatchers("/city/*").authenticated()
//                .antMatchers("/country/*").authenticated()
//                .antMatchers("/plane/*").authenticated()
//                .antMatchers("/flight/*").authenticated()
//                .antMatchers("/airport/*").authenticated()
//                .antMatchers("/company/*").authenticated()
//                .antMatchers("/seat/*").authenticated()
//                .antMatchers("/order/*").authenticated()
//                .antMatchers("/user/*").authenticated()
//                .antMatchers("/city/add*").hasRole("ADMIN")
//                .antMatchers("/country/add*").hasRole("ADMIN")
//                .antMatchers("/plane/add*").hasRole("ADMIN")
//                .antMatchers("/flight/add*").hasRole("ADMIN")
//                .antMatchers("/airport/add*").hasRole("ADMIN")
//                .antMatchers("/company/add*").hasRole("ADMIN")
//                .antMatchers("/seat/add*").hasRole("ADMIN")
//                .antMatchers("/order/add*").hasRole("ADMIN")
//                .antMatchers("/user/add*").hasRole("ADMIN")
//            .and()
//                .formLogin()
//                .loginPage("/login")
//                .defaultSuccessUrl("/home")
//                .failureUrl("/login?error")
//                .usernameParameter("username")
//                .passwordParameter("password")
//            .and()
//            .logout()
//                    .logoutSuccessUrl("/login?logout");

    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter(){
        JwtAccessTokenConverter converter=new JwtAccessTokenConverter();
        converter.setSigningKey(signingKey);
        return converter;
    }

    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices(){
        DefaultTokenServices defaultTokenServices=new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }
}
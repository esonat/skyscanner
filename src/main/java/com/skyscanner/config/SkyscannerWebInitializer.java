package com.skyscanner.config;

import com.skyscanner.config.web.WebConfig;
import com.skyscanner.config.web.WebDatabaseConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

@Order(1)
public class SkyscannerWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

    Logger logger= LoggerFactory.getLogger(SkyscannerWebInitializer.class);

    protected Class<?>[] getRootConfigClasses(){
        return new Class<?>[] {SkyscannerBeanConfiguration.class,WebSecurityConfig.class,WebDatabaseConfig.class};
    }

    protected Class<?>[] getServletConfigClasses(){
        return new Class<?>[] {WebConfig.class};
    }

    protected String[] getServletMappings(){
        return new String[] {"/"};
    }
}
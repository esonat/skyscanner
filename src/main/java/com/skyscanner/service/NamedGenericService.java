package com.skyscanner.service;

public interface NamedGenericService<T> extends GenericService<T>{
    T getByName(String name);
}

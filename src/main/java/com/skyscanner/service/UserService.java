package com.skyscanner.service;

import com.skyscanner.domain.SkyscannerUser;

public interface UserService extends GenericService<SkyscannerUser> {
    //SkyscannerUser get(int id) throws Exception;
    //void update(SkyscannerUser user) throws Exception;
    //List<SkyscannerUser> getAll() throws Exception;
    SkyscannerUser getUserByFullname(String fullname) throws Exception;
    SkyscannerUser getUserByUsername(String username) throws Exception;
}

package com.skyscanner.service;

import com.skyscanner.domain.Plane;

public interface PlaneService extends GenericService<Plane>{

}

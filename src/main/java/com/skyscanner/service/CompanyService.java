package com.skyscanner.service;

import com.skyscanner.domain.Company;

public interface CompanyService extends GenericService<Company>{
    Company findByName(String name);
}

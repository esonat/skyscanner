package com.skyscanner.service.impl;

import com.skyscanner.dao.OrderDao;
import com.skyscanner.domain.Order;
import com.skyscanner.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;


    @Override
    public void save(Order item) throws Exception {
        orderDao.save(item);
    }

    @Override
    public List<Order> findByOrderType(boolean orderType) throws Exception {
        return orderDao.findByOrderType(orderType);
    }

    @Override
    public Order get(int id) throws Exception {
        return orderDao.get(id);
    }

    @Override
    public List<Order> getAll() throws Exception {
        return orderDao.getAll();
    }

    @Override
    public void update(Order item) throws Exception {
        orderDao.save(item);
    }

    @Override
    public void delete(Order item) throws Exception {
        orderDao.delete(item);
    }
}

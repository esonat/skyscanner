package com.skyscanner.service.impl;

import com.skyscanner.dao.AirportDao;
import com.skyscanner.domain.Airport;
import com.skyscanner.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("airportService")
public class AirportServiceImpl implements AirportService {

    @Autowired
    private AirportDao airportDao;

    @Override
    public void save(Airport item) throws Exception{
        airportDao.save(item);
    }

    @Override
    public Airport get(int id) throws Exception {
        return airportDao.get(id);
    }

    @Override
    public List<Airport> getAll() throws Exception {
        return airportDao.getAll();
    }

    @Override
    public void update(Airport item) throws Exception {
        airportDao.save(item);
    }

    @Override
    public void delete(Airport item) throws Exception {
        airportDao.delete(item);
    }

    @Override
    public Airport findByName(String name) throws Exception {
        return airportDao.findByName(name);
    }
}

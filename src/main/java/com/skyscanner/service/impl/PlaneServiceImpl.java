package com.skyscanner.service.impl;

import com.skyscanner.domain.Plane;
import com.skyscanner.service.PlaneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("planeService")
public class PlaneServiceImpl implements PlaneService {
    @Autowired
    private PlaneService planeService;

    @Override
    public void save(Plane item) throws Exception {
        planeService.save(item);
    }

    @Override
    public Plane get(int id) throws Exception {
        return planeService.get(id);
    }

    @Override
    public List<Plane> getAll() throws Exception {
        return planeService.getAll();
    }

    @Override
    public void update(Plane item) throws Exception {
        planeService.update(item);
    }

    @Override
    public void delete(Plane item) throws Exception {
        planeService.delete(item);
    }
}

package com.skyscanner.service.impl;

import com.skyscanner.dao.SeatDao;
import com.skyscanner.domain.Seat;
import com.skyscanner.service.SeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("seatService")
public class SeatServiceImpl implements SeatService {

    @Autowired
    private SeatDao seatDao;

    @Override
    public void save(Seat item) throws Exception {
        seatDao.save(item);
    }

    @Override
    public Seat findByRowAndColumn(String row, int column) throws Exception {
        return seatDao.findByRowAndColumn(row,column);
    }

    @Override
    public Seat get(int id) throws Exception {
        return seatDao.get(id);
    }

    @Override
    public List<Seat> getAll() throws Exception {
        return seatDao.getAll();
    }

    @Override
    public void update(Seat item) throws Exception {
        seatDao.save(item);
    }

    @Override
    public void delete(Seat item) throws Exception {
        seatDao.delete(item);
    }
}

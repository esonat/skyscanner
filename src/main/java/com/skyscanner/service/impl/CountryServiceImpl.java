package com.skyscanner.service.impl;

import com.skyscanner.dao.CountryDao;
import com.skyscanner.domain.Country;
import com.skyscanner.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("countryService")
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryDao countryDao;

    @Override
    public void save(Country item) throws Exception {
        countryDao.save(item);
    }

    @Override
    public Country findByName(String name) throws Exception {
        return countryDao.getByName(name);
    }

    @Override
    public Country get(int id) throws Exception {
        return countryDao.get(id);
    }

    @Override
    public List<Country> getAll() throws Exception {
        return countryDao.getAll();
    }

    @Override
    public void update(Country item) throws Exception {
        countryDao.save(item);
    }

    @Override
    public void delete(Country item) throws Exception {
        countryDao.delete(item);
    }
}

package com.skyscanner.service.impl;

import com.skyscanner.domain.Company;
import com.skyscanner.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyService companyService;

    @Override
    public void save(Company item) throws Exception {
        companyService.save(item);
    }

    @Override
    public Company get(int id) throws Exception {
        return companyService.get(id);
    }

    @Override
    public List<Company> getAll() throws Exception {
        return companyService.getAll();
    }

    @Override
    public void update(Company item) throws Exception {
        companyService.update(item);
    }

    @Override
    public void delete(Company item) throws Exception {
        companyService.delete(item);
    }

    @Override
    public Company findByName(String name) {
        return companyService.findByName(name);
    }
}

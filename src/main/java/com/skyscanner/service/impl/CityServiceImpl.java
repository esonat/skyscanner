package com.skyscanner.service.impl;

import com.skyscanner.dao.CityDao;
import com.skyscanner.domain.City;
import com.skyscanner.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("cityService")
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao cityDao;

    @Override
    public void save(City item) throws Exception {
        cityDao.save(item);
    }

    @Override
    public City findByName(String name) throws Exception {
        return cityDao.getByName(name);
    }

    @Override
    public City get(int id) throws Exception {
        return cityDao.get(id);
    }

    @Override
    public List<City> getAll() throws Exception {
        return cityDao.getAll();
    }

    @Override
    public void update(City item) throws Exception {
        cityDao.save(item);
    }

    @Override
    public void delete(City item) throws Exception {
        cityDao.delete(item);
    }
}

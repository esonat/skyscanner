package com.skyscanner.service.impl;

import com.skyscanner.dao.UserDao;
import com.skyscanner.domain.SkyscannerUser;
import com.skyscanner.exception.UserNotFoundException;
import com.skyscanner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value="userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public void save(SkyscannerUser item) throws Exception {
        userDao.save(item);
    }

    @Override
    public SkyscannerUser get(int id) throws Exception{
        return userDao.get(id);
    }

    @Override
    public List<SkyscannerUser> getAll() throws Exception{
        return userDao.getAll();
    }

    @Override
    public SkyscannerUser getUserByFullname(String fullname) throws UserNotFoundException {
        return userDao.getUserByFullname(fullname);
    }

    @Override
    public SkyscannerUser getUserByUsername(String username) throws UserNotFoundException{
        return userDao.getUserByUserName(username);
    }

    @Override
    public void update(SkyscannerUser user) throws Exception{
        userDao.save(user);
    }

    @Override
    public void delete(SkyscannerUser user) throws Exception{
        userDao.delete(user);
    }
}

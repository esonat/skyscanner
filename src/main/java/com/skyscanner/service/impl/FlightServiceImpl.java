package com.skyscanner.service.impl;

import com.skyscanner.dao.FlightDao;
import com.skyscanner.domain.Flight;
import com.skyscanner.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("flightService")
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightDao flightDao;

    @Override
    public void save(Flight item) throws Exception {
        flightDao.save(item);
    }

    @Override
    public Flight get(int id) throws Exception {
        return flightDao.get(id);
    }

    @Override
    public List<Flight> getAll() throws Exception {
        return flightDao.getAll();
    }

    @Override
    public void update(Flight item) throws Exception {
        flightDao.save(item);
    }

    @Override
    public void delete(Flight item) throws Exception {
        flightDao.delete(item);
    }
}

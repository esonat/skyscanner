package com.skyscanner.service;

import com.skyscanner.domain.City;

public interface CityService extends GenericService<City>{
    City findByName(String name) throws Exception;
}

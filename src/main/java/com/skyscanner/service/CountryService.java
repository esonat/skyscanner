package com.skyscanner.service;

import com.skyscanner.domain.Country;

public interface CountryService extends GenericService<Country>{
    Country findByName(String name) throws Exception;
}

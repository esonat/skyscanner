package com.skyscanner.service;

import com.skyscanner.domain.Flight;

public interface FlightService extends GenericService<Flight> {

}

package com.skyscanner.service;

import java.util.List;

public interface GenericService<T> {
    T get(int id) throws Exception;
    void save(T item) throws Exception;
    List<T> getAll() throws Exception;
    void update(T item) throws Exception;
    void delete(T item) throws Exception;
}

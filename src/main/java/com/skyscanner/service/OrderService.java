package com.skyscanner.service;


import com.skyscanner.domain.Order;

import java.util.List;

public interface OrderService extends GenericService<Order> {
    List<Order> findByOrderType(boolean orderType) throws Exception;
}

package com.skyscanner.service;

import com.skyscanner.domain.Seat;

public interface SeatService extends GenericService<Seat>{
    Seat findByRowAndColumn(String row,int column) throws Exception;
}

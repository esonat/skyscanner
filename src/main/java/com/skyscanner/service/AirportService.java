package com.skyscanner.service;

import com.skyscanner.domain.Airport;

public interface AirportService extends GenericService<Airport>{
    Airport findByName(String name) throws Exception;
}

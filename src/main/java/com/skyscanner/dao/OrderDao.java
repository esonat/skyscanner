package com.skyscanner.dao;

import com.skyscanner.domain.Order;

import java.util.List;

public interface OrderDao extends GenericDao<Order> {
    List<Order> findByOrderType(boolean orderType) throws Exception;
}

package com.skyscanner.dao;

import com.skyscanner.domain.Airport;

public interface AirportDao extends GenericDao<Airport>{
    Airport findByName(String name) throws Exception;
}

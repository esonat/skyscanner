package com.skyscanner.dao;

import com.skyscanner.domain.Plane;

public interface PlaneDao extends GenericDao<Plane> {

}

package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.PlaneDao;
import com.skyscanner.domain.Plane;
import org.springframework.stereotype.Repository;

@Repository("planeDao")
public class PlaneDaoHibernate extends GenericDaoHibernate<Plane> implements PlaneDao{
}

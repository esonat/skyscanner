package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.OrderDao;
import com.skyscanner.domain.Country;
import com.skyscanner.domain.Order;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("orderDao")
public class OrderDaoHibernate extends GenericDaoHibernate<Order> implements OrderDao {
    @Autowired
    SessionFactory sessionFactory;

    protected Log log = LogFactory.getLog(CountryDaoHibernate.class);

    public OrderDaoHibernate() {
        super(Order.class);
    }

    @Override
    public List<Order> findByOrderType(boolean orderType) throws Exception {
        Session session=sessionFactory.openSession();
        log.info("findByOrderType called.orderType="+orderType);

        Query query=session.createQuery("From Order o WHERE o.orderType = :orderType");
        query.setParameter("orderType", orderType);

        List<Order> result=query.list();

        if(result ==null
                || result.size()==0) {
            throw new Exception("No order found with the orderType: "+ orderType);
        }

        session.close();
        return result;
    }
}

package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.UserDao;
import com.skyscanner.domain.SkyscannerUser;
import com.skyscanner.exception.UserNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public class UserDaoHibernate extends GenericDaoHibernate<SkyscannerUser> implements UserDao {

    @Autowired
    SessionFactory sessionFactory;

    protected Log log = LogFactory.getLog(UserDaoHibernate.class);

    public UserDaoHibernate() {
        super(SkyscannerUser.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public SkyscannerUser getUserByFullname(String fullname) {
        Session session=sessionFactory.openSession();
        log.info("getUserByFullname called.Fullname="+fullname);

        Query query=session.createQuery("From SkyscannerUser u WHERE concat(u.Firstname, ' ', u.Middlename,' ',u.Lastname)= :fullname");
        query.setParameter("fullname", fullname);

        List<SkyscannerUser> result=query.list();

        if(result ==null
                || result.size()==0) {
            throw new UserNotFoundException("No user found with the name: "+ fullname);
        }

        session.close();
        return result.get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public SkyscannerUser getUserByUserName(String username) {
        Session session=sessionFactory.openSession();

        Query query=session.createQuery("From SkyscannerUser where username= :username");
        query.setParameter("username", username);

        List<SkyscannerUser> result=query.list();

        if(result==null
                || result.size()==0){
            throw new UserNotFoundException("No user found with the username: "+username);
        }

        session.close();
        return result.get(0);
    }
}
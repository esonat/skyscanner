package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.CountryDao;
import com.skyscanner.domain.City;
import com.skyscanner.domain.Country;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("countryDao")
public class CountryDaoHibernate extends GenericDaoHibernate<Country> implements CountryDao {

    @Autowired
    SessionFactory sessionFactory;

    protected Log log = LogFactory.getLog(CountryDaoHibernate.class);

    public CountryDaoHibernate() {
        super(Country.class);
    }

    @Override
    public Country getByName(String name) throws Exception {
        Session session=sessionFactory.openSession();
        log.info("getByName called.name="+name);

        Query query=session.createQuery("From Country c WHERE c.name = :name");
        query.setParameter("name", name);

        List<Country> result=query.list();

        if(result ==null
                || result.size()==0) {
            throw new Exception("No country found with the name: "+ name);
        }


        session.close();
        return result.get(0);
    }

    @Override
    public Country getByCode(String code) throws Exception {
        Session session=sessionFactory.openSession();
        log.info("getByCode called.name="+code);

        Query query=session.createQuery("From Country c WHERE c.code = :code");
        query.setParameter("code", code);

        List<Country> result=query.list();

        if(result ==null
                || result.size()==0) {
            throw new Exception("No country found with the code: "+ code);
        }

        session.close();
        return result.get(0);
    }
}

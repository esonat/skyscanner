package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.FlightDao;
import com.skyscanner.domain.Flight;
import org.springframework.stereotype.Repository;

@Repository("flightDao")
public class FlightDaoHibernate extends GenericDaoHibernate<Flight> implements FlightDao {

}

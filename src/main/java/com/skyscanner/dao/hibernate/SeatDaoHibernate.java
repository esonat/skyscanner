package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.SeatDao;
import com.skyscanner.domain.Country;
import com.skyscanner.domain.Seat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("seatDao")
public class SeatDaoHibernate extends GenericDaoHibernate<Seat> implements SeatDao {

    @Autowired
    SessionFactory sessionFactory;

    protected Log log = LogFactory.getLog(CountryDaoHibernate.class);

    public SeatDaoHibernate() {
        super(Seat.class);
    }

    @Override
    public Seat findByRowAndColumn(String row, int column) throws Exception {
        Session session=sessionFactory.openSession();
        log.info("findByRowAndColumn called.Row="+row+" column="+column);

        Query query=session.createQuery("From Seat s WHERE s.row = :row AND s.column = :column");
        query.setParameter("row",row);
        query.setParameter("column",column);

        List<Seat> result=query.list();

        if(result==null
            || result.size()==0){
            throw new Exception("No seat found with row:"+row +" and column:"+column);
        }

        session.close();
        return result.get(0);
    }
}

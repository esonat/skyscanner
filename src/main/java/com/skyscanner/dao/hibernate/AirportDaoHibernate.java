package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.AirportDao;
import com.skyscanner.domain.Airport;
import com.skyscanner.domain.City;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("airportDao")
public class AirportDaoHibernate extends GenericDaoHibernate<Airport> implements AirportDao {

    @Autowired
    SessionFactory sessionFactory;

    protected Log log = LogFactory.getLog(AirportDaoHibernate.class);

    public AirportDaoHibernate() {
        super(Airport.class);
    }

    @Override
    public Airport findByName(String name) throws Exception {
        Session session=sessionFactory.openSession();
        log.info("findByName called.name="+name);

        Query query=session.createQuery("From Airport a WHERE a.name = :name");
        query.setParameter("name", name);

        List<Airport> result=query.list();

        if(result ==null
                || result.size()==0) {
            throw new Exception("No airport found with the name: "+ name);
        }

        session.close();
        return result.get(0);
    }
}

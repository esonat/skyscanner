package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.CompanyDao;
import com.skyscanner.domain.Company;
import com.skyscanner.domain.Plane;
import com.skyscanner.domain.SkyscannerUser;
import com.skyscanner.exception.UserNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("companyDao")
public class CompanyDaoHibernate extends GenericDaoHibernate<Company> implements CompanyDao {

    @Autowired
    SessionFactory sessionFactory;

    protected Log log = LogFactory.getLog(CompanyDaoHibernate.class);

    public CompanyDaoHibernate() {
        super(Company.class);
    }

    @Override
    public Company getByName(String name) throws Exception {
        Session session=sessionFactory.openSession();
        log.info("getByName called.name="+name);

        Query query=session.createQuery("From Company c WHERE c.name = :name");
        query.setParameter("name", name);

        List<Company> result=query.list();

        if(result ==null
                || result.size()==0) {
            throw new Exception("No company found with the name: "+ name);
        }

        session.close();
        return result.get(0);
    }
}

package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.GenericDao;
import com.skyscanner.domain.DomainObject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


public class GenericDaoHibernate<T extends DomainObject> implements GenericDao<T> {
    private Class<T> type;

    public GenericDaoHibernate(){
        super();
    }

    @Autowired
    SessionFactory sessionFactory;

    public GenericDaoHibernate(Class<T> type) {
        super();
        this.type = type;
    }


    public void delete(T object) throws Exception{
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(object);
        session.getTransaction().commit();
        session.close();
    }

    public T get(int id) throws Exception {
        //SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        T entity = session.get(type, id);
        return entity;
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() throws Exception{
        //SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        List<T> result = (ArrayList<T>) session.createCriteria(type).list();
        session.close();
        return result;
    }

    public void save(T object) throws Exception{
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(object);

        session.getTransaction().commit();
        session.close();
    }
}
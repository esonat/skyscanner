package com.skyscanner.dao.hibernate;

import com.skyscanner.dao.CityDao;
import com.skyscanner.domain.City;
import com.skyscanner.domain.Company;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("cityDao")
public class CityDaoHibernate extends GenericDaoHibernate<City> implements CityDao {

    @Autowired
    SessionFactory sessionFactory;

    protected Log log = LogFactory.getLog(CityDaoHibernate.class);

    public CityDaoHibernate() {
        super(City.class);
    }

    @Override
    public City getByName(String name) throws Exception {
        Session session=sessionFactory.openSession();
        log.info("getByName called.name="+name);

        Query query=session.createQuery("From City c WHERE c.name = :name");
        query.setParameter("name", name);

        List<City> result=query.list();

        if(result ==null
                || result.size()==0) {
            throw new Exception("No city found with the name: "+ name);
        }

        session.close();
        return result.get(0);
    }
}

package com.skyscanner.dao;

import com.skyscanner.domain.Flight;

public interface FlightDao extends GenericDao<Flight> {

}

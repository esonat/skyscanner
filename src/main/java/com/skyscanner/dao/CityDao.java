package com.skyscanner.dao;

import com.skyscanner.domain.City;

public interface CityDao extends GenericDao<City> {
    City getByName(String name) throws Exception;
}

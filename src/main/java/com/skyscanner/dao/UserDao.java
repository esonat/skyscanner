package com.skyscanner.dao;

import com.skyscanner.domain.SkyscannerUser;

public interface UserDao extends GenericDao<SkyscannerUser>{
    SkyscannerUser getUserByFullname(String name);
    SkyscannerUser getUserByUserName(String username);
}

package com.skyscanner.dao;

import com.skyscanner.domain.Company;

public interface CompanyDao extends GenericDao<Company>{
    Company getByName(String name) throws Exception;
}

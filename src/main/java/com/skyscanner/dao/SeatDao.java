package com.skyscanner.dao;

import com.skyscanner.domain.Seat;

public interface SeatDao extends GenericDao<Seat>{
    Seat findByRowAndColumn(String row,int column) throws Exception;
}

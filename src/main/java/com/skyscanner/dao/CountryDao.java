package com.skyscanner.dao;

import com.skyscanner.domain.Country;

public interface CountryDao extends GenericDao<Country> {
    Country getByName(String name) throws Exception;
    Country getByCode(String code) throws Exception;
}

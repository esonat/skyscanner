package com.skyscanner.dao;

import com.skyscanner.domain.DomainObject;

import java.util.List;

public interface GenericDao<T extends DomainObject> {
    public void delete(T object) throws Exception;

    public T get(int id) throws  Exception;

    public List<T> getAll() throws Exception;

    public void save(T object) throws Exception;
}
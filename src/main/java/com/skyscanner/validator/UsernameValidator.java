package com.skyscanner.validator;

import com.skyscanner.domain.SkyscannerUser;
import com.skyscanner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<Username,String>{
    @Autowired
    private UserService userService;

    public void initialize(Username constraintAnnotation){
    }

    public boolean isValid(String value,ConstraintValidatorContext context){
        SkyscannerUser user=null;
        try{
            user=userService.getUserByUsername(value);
        }catch(Exception e){
            return true;
        }
        if(user!=null){
            return false;
        }
        return true;
    }
}
package com.skyscanner.aop;
import com.skyscanner.dao.hibernate.AirportDaoHibernate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceAspect {

    protected Log log = LogFactory.getLog(AirportDaoHibernate.class);

    @Around("@annotation(org.springframework.stereotype.Service)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable{
        final long start=System.currentTimeMillis();

        final Object proceed=joinPoint.proceed();

        final long executionTime=System.currentTimeMillis()-start;

        log.info(joinPoint.getSignature()+" executed in "+executionTime+"ms");
        return proceed;
    }
}
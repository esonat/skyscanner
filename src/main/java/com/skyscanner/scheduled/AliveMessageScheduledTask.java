package com.skyscanner.scheduled;

import com.skyscanner.domain.RestAPI;
import com.skyscanner.util.TimestampUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Component
public class AliveMessageScheduledTask {
//    @Autowired
//    private ApplicationContext context;
//
//    private Environment env=context.getEnvironment();

    Logger logger= Logger.getLogger(AliveMessageScheduledTask.class);

    @Value("data-service")
    private String applicationId;

    //=Integer.parseInt(env.getProperty("application.id"));

    @Value("${application.host}")
    private String applicationHost;
    //=env.getProperty("application.host");

    @Value("${server.port}")
    private int applicationPort;
    //=Integer.parseInt(env.getProperty("application.port"));

    @Value("${discovery-service.url}")
    private String discoveryServiceUrl;
    //=env.getProperty("discovery-service.url");

    @Scheduled(fixedRate = 3000)
    public void scheduleTask(){
        try {

            RestTemplate restTemplate = new RestTemplate();
            RestAPI entity = new RestAPI(applicationId, applicationHost, applicationPort, true, TimestampUtil.GetTimestamp());

            logger.info("Sending " + entity.toString());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<RestAPI> httpEntity = new HttpEntity<RestAPI>(entity, headers);
            ResponseEntity<String> response = restTemplate.postForEntity(discoveryServiceUrl, httpEntity, String.class);
        }catch (ResourceAccessException e){
            logger.error(e.getMessage());
            return;
        }catch (Exception  e){
            logger.error(e.getMessage());
            return;
        }
        //ResponseEntity<String> result=restTemplate.exchange(discoveryServiceUrl, HttpMethod.POST,entity,String.class);
    }
}

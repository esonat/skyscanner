// package com.skyscanner.rest;
//
// import com.github.springtestdbunit.DbUnitTestExecutionListener;
// import com.github.springtestdbunit.annotation.DatabaseSetup;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.test.context.ContextConfiguration;
// import org.springframework.test.context.TestExecutionListeners;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
// import org.springframework.test.web.servlet.MockMvc;
//
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration
// @AutoConfigureMockMvc
// @SpringBootTest
// @TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
// DbUnitTestExecutionListener.class})
// public class UserControllerUnitTest {
//
//     @Autowired
//     private MockMvc mockMvc;
//
//     private static final String CONTENT_TYPE="application/json;charset=UTF-8";
//
//
//     @Test
//     @DatabaseSetup("sampleData.xml")
//     public void testGetAll() throws Exception{
//         this.mockMvc.perform(get("/data-service/v1/users")).andExpect(status().isOk())
//                 .andExpect(content().json("[{\"id\":1,\"firstname\":\"engin\",\"middlename\",\"lastname\":\"sonat\",\"username\":\"engin\",\"password\":\"sonat\"},"
//                         +"{\"id\":2,\"firstname\":\"admin\",\"middlename\",\"lastname\":\"administrator\",\"username\":\"admin\",\"password\":\"password\"}"));
//
//     }
//
// }
